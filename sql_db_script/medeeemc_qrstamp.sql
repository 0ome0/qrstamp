-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 12, 2017 at 03:04 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medeeemc_qrstamp`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_badge`
--

CREATE TABLE `t_badge` (
  `badge_id` int(11) NOT NULL,
  `badge_name` varchar(255) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `badge_template_id` int(11) NOT NULL,
  `number_of_badge` int(11) NOT NULL,
  `link_qr_code` varchar(255) NOT NULL,
  `badge_status` int(11) NOT NULL,
  `badge_information` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `badge_value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_promotion`
--

CREATE TABLE `t_promotion` (
  `promo_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `promo_name` varchar(255) NOT NULL,
  `promo_start_date` date NOT NULL,
  `promo_end_date` date NOT NULL,
  `promo_badge_qty` int(11) NOT NULL,
  `promo_status` int(11) NOT NULL,
  `promo_condition` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_shop`
--

CREATE TABLE `t_shop` (
  `shop_id` int(11) NOT NULL,
  `shop_cat_id` int(11) NOT NULL,
  `shop_name` text NOT NULL,
  `shop_address` text NOT NULL,
  `shop_lat` float NOT NULL,
  `shop_long` float NOT NULL,
  `shop_mobile_no` varchar(10) NOT NULL,
  `shop_service_hours` text NOT NULL,
  `shop_information` text NOT NULL,
  `shop_status` int(11) NOT NULL,
  `shop_level` int(11) NOT NULL,
  `shop_social_fb` text NOT NULL,
  `shop_social_line` int(11) NOT NULL,
  `shop_logo` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_shop`
--

INSERT INTO `t_shop` (`shop_id`, `shop_cat_id`, `shop_name`, `shop_address`, `shop_lat`, `shop_long`, `shop_mobile_no`, `shop_service_hours`, `shop_information`, `shop_status`, `shop_level`, `shop_social_fb`, `shop_social_line`, `shop_logo`, `date_created`) VALUES
(1, 1, '<p>\r\n	BD120 Shop</p>\r\n', '<p>\r\n	Rama9</p>\r\n', 0, 0, '085133018', '', '', 1, 1, '', 0, '', '2017-09-11 14:18:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_badge`
--
ALTER TABLE `t_badge`
  ADD PRIMARY KEY (`badge_id`);

--
-- Indexes for table `t_promotion`
--
ALTER TABLE `t_promotion`
  ADD PRIMARY KEY (`promo_id`);

--
-- Indexes for table `t_shop`
--
ALTER TABLE `t_shop`
  ADD PRIMARY KEY (`shop_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_badge`
--
ALTER TABLE `t_badge`
  MODIFY `badge_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_promotion`
--
ALTER TABLE `t_promotion`
  MODIFY `promo_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_shop`
--
ALTER TABLE `t_shop`
  MODIFY `shop_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
