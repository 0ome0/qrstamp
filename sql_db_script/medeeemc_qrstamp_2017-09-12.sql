-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 12, 2017 at 03:36 PM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medeeemc_qrstamp`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_shop_level`
--

CREATE TABLE `t_shop_level` (
  `shop_level_id` int(11) NOT NULL,
  `shop_level_name` varchar(255) NOT NULL,
  `license_days` int(11) NOT NULL,
  `shop_status` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_shop_level`
--

INSERT INTO `t_shop_level` (`shop_level_id`, `shop_level_name`, `license_days`, `shop_status`, `date_created`) VALUES
(1, 'ทดลองใช้งาน 30 วัน', 30, 1, '2017-09-12 03:41:02'),
(2, 'ทั่วไป 1 ปี', 365, 1, '2017-09-12 03:41:02'),
(3, 'VIP', 999999999, 1, '2017-09-12 03:41:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_shop_level`
--
ALTER TABLE `t_shop_level`
  ADD PRIMARY KEY (`shop_level_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_shop_level`
--
ALTER TABLE `t_shop_level`
  MODIFY `shop_level_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
