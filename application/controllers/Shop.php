<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shop extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
    }
    
    function shop_management() {			
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('t_shop');
        $crud->set_subject('Shop');
        // $crud->required_fields('username');
        
        $crud->set_field_upload('shop_logo','assets/uploads/shop_logo');
        $crud->field_type('date_created', 'hidden');

        $crud->unset_add();
        $crud->unset_delete();
        
        $crud->set_relation('shop_cat_id','t_shop_category','shop_cat_name');
        $crud->set_relation('shop_status','t_status','status_name');
        $crud->set_relation('shop_level','t_shop_level','shop_level_name');

        $crud->columns('shop_logo','shop_name','shop_mobile_no','shop_status','shop_level');
        $output = $crud->render();
        // $this->load->view('admin_header', $output); //<-- This line of code changed
                    
        $this->load->view('shop', $output);																						
        // $this->load->view('administrator/includes/admin_footer');
    }    

    function promotion_management() {			
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('t_promotion');
        $crud->set_subject('Promotion');
        
        $crud->set_field_upload('promo_image','assets/uploads/promo_image');

        $crud->set_relation('promo_status','t_status','status_name');
        $crud->columns('promo_image','promo_name','promo_start_date','promo_end_date','promo_badge_qty','promo_status');
        $output = $crud->render();
        // $this->load->view('admin_header', $output); //<-- This line of code changed
                    
        $this->load->view('promotion', $output);																						
        // $this->load->view('administrator/includes/admin_footer');
    }  

    function badge_management() {			
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('t_badge');
        $crud->set_subject('Badge');
        
        $crud->set_relation('badge_status','t_status','status_name');
        $crud->set_relation('badge_template_id','t_badge_template','badge_template_name');
        $crud->columns('badge_template_id','badge_name','number_of_badge','badge_status');
        $output = $crud->render();
        // $this->load->view('admin_header', $output); //<-- This line of code changed
                    
        $this->load->view('badge', $output);																						
        // $this->load->view('administrator/includes/admin_footer');
    }  

    function badge_template_management() {			
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('t_badge_template');
        $crud->set_subject('Badge Template');
        
        $crud->set_field_upload('image_before_stamp','assets/uploads/badge_template');
        $crud->set_field_upload('image_after_stamp','assets/uploads/badge_template');

        $crud->set_relation('badge_template_status','t_status','status_name');
        $crud->columns('badge_template_name','image_before_stamp','image_after_stamp','badge_template_status');
        $output = $crud->render();
        // $this->load->view('admin_header', $output); //<-- This line of code changed
                    
        $this->load->view('badge_template', $output);																						
        // $this->load->view('administrator/includes/admin_footer');
    }  

    function shop_category_management() {			
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('t_shop_category');
        $crud->set_subject('Shop Category');
        
        $crud->set_relation('shop_cat_status','t_status','status_name');
        $crud->columns('shop_cat_name','shop_cat_status');
        $output = $crud->render();
        // $this->load->view('admin_header', $output); //<-- This line of code changed
                    
        $this->load->view('shop_category', $output);																						
        // $this->load->view('administrator/includes/admin_footer');
    }  

    function shop_level_management() {			
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('t_shop_level');
        $crud->set_subject('Shop Level');
        
        $crud->set_relation('shop_status','t_status','status_name');
        $crud->columns('shop_level_name','license_days','shop_status');
        $output = $crud->render();
        // $this->load->view('admin_header', $output); //<-- This line of code changed
                    
        $this->load->view('shop_level', $output);																						
        // $this->load->view('administrator/includes/admin_footer');
    }  
    
    
}
