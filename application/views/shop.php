<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
</head>
<body>
	<div>
		<a href='<?php echo site_url('Shop/shop_management')?>'>Shop</a> |
		<a href='<?php echo site_url('Shop/promotion_management')?>'>Promotion</a> |
		<a href='<?php echo site_url('Shop/badge_management')?>'>Badge</a> |
		<a href='<?php echo site_url('Shop/badge_template_management')?>'>Badge Template</a> |
		<a href='<?php echo site_url('Shop/shop_category_management')?>'>Shop Category</a> |
		<a href='<?php echo site_url('Shop/shop_level_management')?>'>Shop Level</a> |
		<!-- <a href='<?php echo site_url('examples/orders_management')?>'>Orders</a> | -->
		<!-- <a href='<?php echo site_url('examples/products_management')?>'>Products</a> | -->
		<!-- <a href='<?php echo site_url('examples/offices_management')?>'>Offices</a> |  -->
		<!-- <a href='<?php echo site_url('examples/employees_management')?>'>Employees</a> |		  -->
		<!-- <a href='<?php echo site_url('examples/film_management')?>'>Films</a> | -->
		<!-- <a href='<?php echo site_url('examples/multigrids')?>'>Multigrid [BETA]</a> -->
		
	</div>
	<div style='height:20px;'></div>  
    <div>
	<h1>เมนูการจัดการ ร้านค้า</h1>
		<?php echo $output; ?>
    </div>
</body>
</html>
